from __future__ import print_function
import tdsc.utils
import sys


def message(context=None, *_, **__):
    print(
        "No existing configurations require host level provisioning. As such, this is a diagnostic state.",
        file=sys.stderr,
    )
    if context:
        print("Tags:")
        print(context.tags)

    return True


state = message

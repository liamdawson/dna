import os
from .actions import get_user_opt_dir
from tdsc.utils import download_file, ensure_directory, get_cache_dir, run_command_transparently, defer
from tdsc.prelude import *

dl_source = 'https://telegram.org/dl/desktop/linux'
dl_destination = os.path.join(get_cache_dir('dna'), 'telegram.tar.xz')
extract_destination = get_user_opt_dir()

def install_telegram_desktop():
    if os.path.exists(dl_destination):
        return True
    
    return download_file(dl_source, dl_destination) and run_command_transparently('tar', '-xJf', dl_destination, '--directory', extract_destination)

state = defer(install_telegram_desktop)

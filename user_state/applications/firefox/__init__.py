from . import linux
from tdsc.prelude import *

state = require_tag('linux', describe_action("Install Firefox Developer Edition for user", linux.state))

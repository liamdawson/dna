import os
from tdsc.prelude import *
from tdsc.utils import download_file, get_cache_dir, run_command_transparently, ensure_directory, defer, set_file_text_content, copy_file_content

THIS_FILE_DIR = os.path.dirname(os.path.realpath(__file__))
EXTRACT_DESTINATION = os.path.join(os.path.expandvars('$HOME'), '.local', 'opt', 'mozilla', 'firefox-dev')


def add_linux_desktop_entry():
    source = os.path.join(THIS_FILE_DIR, 'data', 'firefoxde.desktop')
    destination = os.path.join(os.path.expandvars('$HOME'), '.local', 'share', 'applications', 'firefox-developer-edition.desktop')

    with open(source, 'r') as template:
        return set_file_text_content(destination, template.read().format(app_path=EXTRACT_DESTINATION))


def hide_default_firefox_desktop_entry():
    source = os.path.join(THIS_FILE_DIR, 'data', 'firefox.desktop')
    destination = os.path.join(os.path.expandvars('$HOME'), '.local', 'share', 'applications', 'firefox.desktop')

    return copy_file_content(source, destination)


def download_firefox_de():
    DL_SOURCE = 'https://download.mozilla.org/?product=firefox-devedition-latest-ssl&os=linux64&lang=en-US'
    DL_DESTINATION = os.path.join(get_cache_dir('dna'), 'firefox-de.tar.bz2')

    if not os.path.exists(DL_DESTINATION) and download_file(DL_SOURCE, DL_DESTINATION):
        return ensure_directory(EXTRACT_DESTINATION) and run_command_transparently('tar', '-xjf', DL_DESTINATION, '--directory', EXTRACT_DESTINATION, '--strip-components=1')

    return True


state = action(
    defer(download_firefox_de), 
    defer(add_linux_desktop_entry),
    defer(hide_default_firefox_desktop_entry))

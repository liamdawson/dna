from tdsc.prelude import *
from . import firefox, telegram

state = require_tag('gui', action(describe_action("Install Firefox Developer Edition, and configure user-level Firefox preferences", firefox.state), describe_action("Install Telegram desktop for the current user", telegram.state)))

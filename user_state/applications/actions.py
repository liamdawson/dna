import os
from tdsc.utils import ensure_directory

def get_user_opt_dir(*segments):
    opt_dir = os.path.join(os.path.expandvars('$HOME'), '.local', 'opt')
    destination = os.path.join(opt_dir, *segments) if segments else opt_dir
    ensure_directory(destination, 0o700)
    return destination

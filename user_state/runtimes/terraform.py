from tdsc.utils import defer
from .actions import checkout_env_repo

state = defer(checkout_env_repo, "tfenv", "https://github.com/Zordrak/tfenv.git")

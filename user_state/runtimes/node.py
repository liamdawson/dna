from tdsc.utils import defer
from .actions import checkout_env_repo

state = defer(
    checkout_env_repo,
    "nodenv",
    "https://github.com/nodenv/nodenv.git",
    {
        "node-build": "https://github.com/nodenv/node-build.git",
        "nodenv-update": "https://github.com/nodenv/nodenv-update.git",
        "nodenv-package-rehash": "https://github.com/nodenv/nodenv-package-rehash.git",
    },
)

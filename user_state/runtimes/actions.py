import os
from tdsc.utils import run_command_transparently, run_command_opaquely


def user_home_path(*segments):
    return os.path.join(os.path.expandvars("$HOME"), *segments)


def clone_git_repo(source, destination, branch):
    cmd = ["git", "clone", "--recurse-submodules", source, destination]
    if branch:
        cmd.append("--branch")
        cmd.append(branch)

    return run_command_transparently(*cmd)


def pull_git_repo(destination, branch):
    return (
        run_command_opaquely("git", "fetch", cwd=destination)
        and (
            (not branch)
            or run_command_opaquely("git", "checkout", branch, cwd=destination)
        )
        and run_command_opaquely("git", "pull", "--recurse-submodules", cwd=destination)
    )


def clone_or_pull_git_repo(source, destination, branch=None):
    if os.path.exists(destination):
        return pull_git_repo(destination, branch)
    else:
        return clone_git_repo(source, destination, branch)


def ensure_repo_cloned(source, destination, branch=None):
    if not os.path.exists(destination):
        return clone_git_repo(source, destination, branch)
    return True


def checkout_env_repo(name, source, plugins=None):
    plugins = plugins or {}
    env_dir = user_home_path(".{}".format(name))
    plugins_dir = os.path.join(env_dir, "plugins")
    result = ensure_repo_cloned(source, env_dir)

    for key in plugins:
        if result is True:
            result = ensure_repo_cloned(plugins[key], os.path.join(plugins_dir, key))

    return result

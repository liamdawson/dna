from tdsc.utils import defer
from .actions import checkout_env_repo

state = defer(
    checkout_env_repo,
    "rbenv",
    "https://github.com/rbenv/rbenv.git",
    {
        "ruby-build": "https://github.com/rbenv/ruby-build.git",
        "rbenv-each": "https://github.com/rbenv/rbenv-each.git",
        "rbenv-update": "https://github.com/rkh/rbenv-update.git",
    },
)

from tdsc.utils import defer
from .actions import checkout_env_repo

state = defer(checkout_env_repo, "goenv", "https://github.com/syndbg/goenv.git")

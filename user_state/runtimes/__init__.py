from . import java, ruby, python, go, node, terraform, rust
from tdsc.prelude import *

state = action(
    describe_action("Add Java runtime manager", java.state),
    describe_action("Add Ruby runtime manager", ruby.state),
    describe_action("Add Python runtime manager", python.state),
    describe_action("Add Go runtime manager", go.state),
    describe_action("Add Node runtime manager", node.state),
    describe_action("Add Terraform runtime manager", terraform.state),
    describe_action("Add Rust runtime manager", rust.state),
)

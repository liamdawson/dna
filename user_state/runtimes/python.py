from tdsc.utils import defer
from .actions import checkout_env_repo

state = defer(
    checkout_env_repo,
    "pyenv",
    "https://github.com/pyenv/pyenv.git",
    {
        "pyenv-update": "https://github.com/pyenv/pyenv-update",
        "pyenv-implicit": "https://github.com/concordusapps/pyenv-implict.git",
    },
)

from tdsc.prelude import *
from tdsc.utils import defer
from .actions import checkout_env_repo

state = defer(checkout_env_repo, "jenv", "https://github.com/gcuisinier/jenv.git")

from tdsc.prelude import *
from tdsc.utils import defer, run_command_transparently

state = require_tag('linux', defer(
    run_command_transparently,
    'bash',
    '-c',
    "curl https://sh.rustup.rs -sSf | sh -s -- --default-toolchain stable --no-modify-path -y"
))

import os
from tdsc.utils import download_file, defer, ensure_directory, run_command_opaquely

JETBRAINS_DIR = os.path.join(os.path.expandvars("$HOME"), ".local", "opt", "jetbrains")
APP_DL_SOURCE = (
    "https://download.jetbrains.com/toolbox/jetbrains-toolbox-1.9.3935.tar.gz"
)
APP_DL_DESTINATION = os.path.join(JETBRAINS_DIR, "app.tar.gz")


def download_toolbox():
    ensure_directory(JETBRAINS_DIR)
    if not os.path.exists(APP_DL_DESTINATION):
        return download_file(
            APP_DL_SOURCE, APP_DL_DESTINATION
        ) and run_command_opaquely(
            "tar", "-xzf", APP_DL_DESTINATION, "--directory", JETBRAINS_DIR
        )
    return True


state = defer(download_toolbox)

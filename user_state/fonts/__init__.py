import os
from tdsc.prelude import *
from tdsc.utils import download_file, extract_zip, ensure_directory, defer

USER_FONTS_DIR = os.path.join(os.path.expandvars("$HOME"), ".fonts")


def download_google_font(font_name):
    download_destination = os.path.join(USER_FONTS_DIR, "{}.zip".format(font_name))
    font_destination = os.path.join(USER_FONTS_DIR, font_name)
    download_url = "https://fonts.google.com/download?family={}".format(font_name)

    if not os.path.exists(download_destination):
        return (
            download_file(download_url, download_destination)
            and ensure_directory(font_destination)
            and extract_zip(download_destination, font_destination)
        )

    return True


state = action(
    defer(ensure_directory, USER_FONTS_DIR), defer(download_google_font, "Roboto Mono")
)

from tdsc.utils import run_command_transparently, defer
from tdsc.prelude import *
import os

THIS_FILE_DIR = os.path.dirname(os.path.realpath(__file__))
DATA_DIR = os.path.join(THIS_FILE_DIR, "data")


def unstow_dotfiles():
    return run_command_transparently(
        "stow", "-d", DATA_DIR, "-t", os.path.expandvars("$HOME"), "-R", "dotfiles"
    )


state = defer(unstow_dotfiles)

from tdsc.prelude import *
from . import default, bash, dotfiles

state = action(
    default.state,
    describe_action(
        "Ensure Bash is configured to source all user extensions", bash.state
    ),
    describe_action(
        "Ensure dotfiles are unstowed into the user's home directory", dotfiles.state
    ),
)

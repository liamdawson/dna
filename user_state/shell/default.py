import getpass
from tdsc.prelude import *
from tdsc.utils import run_command_transparently, defer


def ensure_zsh_is_default():
    return run_command_transparently(
        "grep", "-q", "^{}.*zsh$".format(getpass.getuser()), "/etc/passwd"
    ) or run_command_transparently("bash", "-c", 'chsh -s "$(which zsh)"')


state = describe_action(
    "Ensure the current user's shell is ZSH by default", defer(ensure_zsh_is_default)
)

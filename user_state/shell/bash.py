from tdsc.utils import run_command_opaquely, defer
import os

BASHRC_PATH = os.path.join(os.path.expandvars("$HOME"), ".bashrc")
SHD_SOURCE_LINE = 'for f in $(find ~/.sh.d/ -type f,l -print | tr "\\n" " "); do source "$f"; done'


def add_source_line_to_bashrc_if_missing():
    if not run_command_opaquely("grep", "-q", SHD_SOURCE_LINE, BASHRC_PATH):
        with open(BASHRC_PATH, "a") as f:
            f.write("\n{}\n".format(SHD_SOURCE_LINE))
    return True


state = defer(add_source_line_to_bashrc_if_missing)

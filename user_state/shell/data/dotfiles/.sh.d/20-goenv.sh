if [[ -d "$HOME/.goenv" ]]
then
  export PATH="$HOME/.goenv/bin:$PATH"
  eval "$($HOME/.goenv/bin/goenv init -)"
fi

export GOPATH="${GOPATH:-"$HOME/workspace/go"}"
export PATH="$PATH:$GOPATH/bin"

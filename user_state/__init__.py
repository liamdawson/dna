from tdsc.prelude import *
from . import gnome, runtimes, shell, fonts, jetbrains, applications

state = action(
    describe_action("Configure shell preferences", shell.state),
    describe_action("Configure preferred language runtimes", runtimes.state),
    describe_action(
        "Ensure GNOME is configured as desired for the current user",
        require_tag("gnome", gnome.state),
    ),
    describe_action(
        "Ensure preferred fonts are installed for the current user",
        require_tag("gui", fonts.state),
    ),
    describe_action(
        "Ensure Jetbrains toolbox is installed",
        # TODO: support macos
        require_tag("linux", require_tag("gui", jetbrains.state)),
    ),
    describe_action(
        "Ensure preferred user-level applications are available",
        applications.state
    )
)

from tdsc.prelude import *
from tdsc.utils import defer
from .actions import enable_gnome_extension

# TODO: ensure extensions dir exists

state = action(
    defer(
        enable_gnome_extension,
        "user-theme@gnome-shell-extensions.gcampax.github.com",
        "34",
    ),
    defer(enable_gnome_extension, "dash-to-panel@jderose9.github.com", "15"),
    defer(
        enable_gnome_extension,
        "alternate-tab@gnome-shell-extensions.gcampax.github.com",
        "38",
    ),
    defer(enable_gnome_extension, "nohotcorner@azuri.free.fr", "17"),
    defer(enable_gnome_extension, "appindicatorsupport@rgcjonas.gmail.com", "22")
)

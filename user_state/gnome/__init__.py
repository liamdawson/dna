from tdsc.prelude import *
from . import extensions, settings, themes


state = require_tag(
    "gnome",
    action(
        describe_action("Set preferred GNOME theme", themes.state),
        describe_action("Enable preferred GNOME extensions", extensions.state),
        describe_action("Enable preferred GNOME settings", settings.state),
    ),
)

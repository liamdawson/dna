from tdsc.prelude import *
from tdsc.utils import defer
from .actions import set_gnome_setting

laptop_settings = action(
    defer(
        set_gnome_setting,
        "org.gnome.desktop.peripherals.touchpad",
        "natural-scroll",
        "false",
    ),
    defer(
        set_gnome_setting,
        "org.gnome.desktop.peripherals.touchpad",
        "tap-to-click",
        "true",
    ),
    defer(
        set_gnome_setting,
        "org.gnome.desktop.interface",
        "show-battery-percentage",
        "true",
    ),
)

state = describe_action("Enable preferred GNOME laptop settings", laptop_settings)

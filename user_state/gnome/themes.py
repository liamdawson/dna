from tdsc.prelude import *
from tdsc.utils import defer
from .actions import enable_gnome_theme, set_gnome_theme

THEME_DOWNLOAD_URL = "".join(
    [
        "https://www.opendesktop.org/p/1193879/startdownload?file_id=1531547359&file_",
        "name=Ultimate-Maia-Green.tar.xz&file_type=application/x-xz&file_size=243284&",
        "url=https%3A%2F%2Fdl.opendesktop.org%2Fapi%2Ffiles%2Fdownloadfile%2Fid%2F153",
        "1547359%2Fs%2F92029f2538a87b9acb41d0d6fa65a17d%2Ft%2F1531657984%2Fu%2F%2FUlt",
        "imate-Maia-Green.tar.xz",
    ]
)

state = describe_action(
    "Theme GNOME as preferred",
    action(
        defer(enable_gnome_theme, "Ultimate-Maia-Green", THEME_DOWNLOAD_URL),
        defer(set_gnome_theme, "Ultimate-Maia-Green"),
    ),
)

import os
from tdsc.utils import download_file, run_command_transparently, extract_zip, ensure_directory

EXTENSIONS_DIR = os.path.join(
        os.path.expandvars("$HOME"),
        ".local",
        "share",
        "gnome-shell",
        "extensions")


def enable_gnome_extension(id, version):
    ensure_directory(EXTENSIONS_DIR)
    url = "https://extensions.gnome.org/extension-data/{id}.v{version}.shell-extension.zip".format(
        id=id, version=version
    )
    dl_destination = os.path.join(
        EXTENSIONS_DIR,
        "{}.zip".format(id),
    )
    extension_destination = os.path.join(
        EXTENSIONS_DIR,
        id,
        "",
    )

    if not os.path.exists(dl_destination):
        if not (
            download_file(url, dl_destination)
            and extract_zip(dl_destination, extension_destination)
        ):
            return False

    return run_command_transparently(
        "gnome-shell-extension-tool", "-e", id, allowed_exit_codes=[0, 1]
    )


def set_gnome_setting(namespace, setting, value):
    return run_command_transparently("gsettings", "set", namespace, setting, value)


def enable_gnome_theme(id, url):
    # TODO
    return True


def set_gnome_theme(id):
    # TODO
    return True

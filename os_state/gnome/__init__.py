from . import apt
from tdsc.prelude import *

state = action(
    require_tag("apt", describe_action("Install Gnome Shell via apt", apt.state))
)

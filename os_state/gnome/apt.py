from tdsc.utils import defer, apt_install

state = defer(apt_install, "gnome-shell", "gnome-session")

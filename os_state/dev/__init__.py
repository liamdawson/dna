from tdsc.prelude import *
from . import apt_packages

state = require_tag(
    "apt",
    describe_action(
        "Install development packages on APT-based systems", apt_packages.state
    ),
)

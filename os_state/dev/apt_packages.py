from tdsc.prelude import *
from tdsc.utils import defer, apt_install

state = action(
    describe_action(
        "Install headless development packages/tools",
        defer(
            apt_install,
            "curl",
            "stow",
            "vim",
            "git",
            "build-essential",
            "w3m",
            "cmake",
            "gdb",
            "zlib1g-dev",
            "libffi-dev",
            "libssl-dev",
            "libbz2-dev",
            "libreadline-dev",
            "libsqlite3-dev",
            "llvm",
            "libncurses5-dev",
            "libncursesw5-dev",
            "xz-utils",
            "tk-dev",
            "zsh",
            "xclip",
            "libfreetype6-dev",
            "libfontconfig1-dev",
            "libegl1-mesa-dev"
        ),
    ),
    require_tag(
        "gui",
        action(
        describe_action(
            "Install GUI development packages/tools", defer(apt_install, "code")
        ),
        defer(
            apt_install,
            "chromium-browser"
        )),
    ),
)

from tdsc.prelude import *
from tdsc.utils import apt_install, defer

state = require_tag("ubuntu:bionic", defer(apt_install, "nvidia-driver-390"))

from tdsc.prelude import *
from . import nvidia_proprietary

state = require_tag(
    "nvidia-proprietary",
    describe_action(
        "Install proprietary Nvidia graphics drivers", nvidia_proprietary.state
    ),
)

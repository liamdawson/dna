from tdsc.prelude import *
from . import apt, gnome, dev, machine

state = describe_action(
    "Configure the machine to meet root-level preferences",
    action(
        require_tag(
            "apt",
            describe_action("Configure APT package management as preferred", apt.state),
        ),
        require_tag(
            "gnome", describe_action("Configure GNOME installation", gnome.state)
        ),
        describe_action("Configure development requirements", dev.state),
        describe_action("Machine specific configuration", machine.state),
    ),
)

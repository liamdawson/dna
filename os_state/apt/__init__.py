from tdsc.prelude import *
from . import standard_sources, update, microsoft

state = describe_action(
    "Set APT standard mirrors, extra sources, and then update/upgrade.",
    action(standard_sources.state, microsoft.state, update.state),
)

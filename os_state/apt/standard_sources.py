from tdsc.prelude import *
from tdsc.utils import (
    apt_install,
    set_ubuntu_apt_mirror,
    get_linux_distro_codename,
    defer,
)


state = action(
    defer(apt_install, "apt-transport-https", "gnupg"),
    require_tag(
        "linux:ubuntu",
        defer(
            set_ubuntu_apt_mirror,
            "https://mirror.aarnet.edu.au/pub/ubuntu/archive/",
            get_linux_distro_codename(),
        ),
    ),
)

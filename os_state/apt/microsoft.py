import os
from tdsc.prelude import *
from tdsc.utils import defer, set_file_text_content, copy_file_content

THIS_FILE_DIRECTORY = os.path.dirname(os.path.realpath(__file__))
SRC_GPG_KEY_PATH = os.path.join(THIS_FILE_DIRECTORY, "data", "microsoft.gpg")
DEST_GPG_KEY_PATH = "/etc/apt/trusted.gpg.d/microsoft.gpg"


def add_vs_code_source(*_, **__):
    return set_file_text_content(
        "/etc/apt/sources.list.d/vscode.list",
        "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main",
    )


state = action(
    defer(copy_file_content, SRC_GPG_KEY_PATH, DEST_GPG_KEY_PATH),
    describe_action("Enable the VS Code repository", add_vs_code_source),
)

from tdsc.prelude import *
from tdsc.utils import defer, apt_update, apt_upgrade

state = action(defer(apt_update), defer(apt_upgrade))

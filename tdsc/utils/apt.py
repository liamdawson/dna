import subprocess
from tdsc.utils import run_command_transparently, set_file_text_content

SOURCES_FILE_TEMPLATE = """
# managed by provisioning scripts

###### Ubuntu Main Repos
deb {mirror} {codename} main restricted universe multiverse 

###### Ubuntu Update Repos
deb http://security.ubuntu.com/ubuntu/ {codename}-security main restricted universe multiverse 
deb {mirror} {codename}-updates main restricted universe multiverse 
deb {mirror} {codename}-proposed main restricted universe multiverse 
deb {mirror} {codename}-backports main restricted universe multiverse 
"""


def _get_package_states(*packages):
    all_packages = set(packages)

    command = ["dpkg", "--get-selections"] + list(packages)

    dpkg_process = subprocess.Popen(
        command, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )

    package_lines = dpkg_process.communicate()[0].decode("utf-8").splitlines()

    if dpkg_process.returncode > 1:
        raise subprocess.CalledProcessError(dpkg_process.returncode, cmd=command)

    installed_package_lines = filter(
        lambda line: line.endswith("install"), package_lines
    )
    installed_packages = set([line.split("\t")[0] for line in installed_package_lines])

    return installed_packages, all_packages.difference(installed_packages)


def apt_upgrade(**kwargs):
    env = {}
    if "interactive" not in kwargs or not kwargs["interactive"]:
        env["DEBIAN_FRONTEND"] = "noninteractive"
    return run_command_transparently("apt-get", "upgrade", "-y", env=env)


def apt_update():
    return run_command_transparently("apt-get", "update", "-y")


def apt_install(*packages, **kwargs):
    command = ["apt-get", "install", "-y"]

    (_, uninstalled) = _get_package_states(*packages)

    if any(uninstalled):
        env = {}
        if "interactive" not in kwargs or not kwargs["interactive"]:
            env["DEBIAN_FRONTEND"] = "noninteractive"

        return run_command_transparently(*(command + list(uninstalled)), env=env)

    # nothing to install
    return True


def apt_remove(*packages, **kwargs):
    command = ["apt-get", "remove", "-y"]

    (installed, _) = _get_package_states(*packages)

    if any(installed):
        env = {}
        if "interactive" not in kwargs or not kwargs["interactive"]:
            env["DEBIAN_FRONTEND"] = "noninteractive"
        return run_command_transparently(*(command + list(installed)), env=env)

    # nothing to uninstall
    return True


def set_ubuntu_apt_mirror(mirror, codename):
    return set_file_text_content(
        "/etc/apt/sources.list",
        SOURCES_FILE_TEMPLATE.format(mirror=mirror, codename=codename),
    )

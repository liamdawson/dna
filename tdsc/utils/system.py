import subprocess
import sys
import os


def _run_command(
    command,
    stdin,
    stdout,
    stderr,
    cwd=None,
    env=None,
    allowed_exit_codes=None,
    shell=False,
):
    allowed_exit_codes = allowed_exit_codes or [0]
    environment_vars = dict(os.environ.copy())
    environment_vars.update(env or {})

    return (
        subprocess.Popen(
            command,
            shell=shell,
            cwd=cwd,
            stdin=stdin,
            stdout=stdout,
            stderr=stderr,
            env=environment_vars,
        ).wait()
        in allowed_exit_codes
    )


def run_command_transparently(*command, **kwargs):
    cwd = kwargs["cwd"] if "cwd" in kwargs else None
    env = kwargs["env"] if "env" in kwargs else None
    allowed_exit_codes = (
        kwargs["allowed_exit_codes"] if "allowed_exit_codes" in kwargs else None
    )
    shell = kwargs["shell"] if "shell" in kwargs else False
    return _run_command(
        command, sys.stdin, sys.stdout, sys.stderr, cwd, env, allowed_exit_codes, shell
    )


def run_command_opaquely(*command, **kwargs):
    cwd = kwargs["cwd"] if "cwd" in kwargs else None
    env = kwargs["env"] if "env" in kwargs else None
    allowed_exit_codes = (
        kwargs["allowed_exit_codes"] if "allowed_exit_codes" in kwargs else None
    )
    shell = kwargs["shell"] if "shell" in kwargs else False
    with open(os.devnull, "r+b") as null:
        return _run_command(
            command, null, null, null, cwd, env, allowed_exit_codes, shell
        )

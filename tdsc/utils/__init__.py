from .archive import extract_zip
from .filesystem import (
    set_file_text_content,
    set_file_binary_content,
    copy_file_content,
    ensure_directory,
    get_cache_dir
)
from .network import download_file
from .system import run_command_transparently, run_command_opaquely
from tdsc.utils.apt import (
    apt_install,
    apt_remove,
    apt_update,
    apt_upgrade,
    set_ubuntu_apt_mirror,
)
from .environment import (
    get_os_release_content,
    get_linux_distro_codename,
    get_linux_distro_version,
    get_linux_distro_id,
)


def noop(returns=True):
    def apply(*_, **__):
        return returns

    return apply


def defer(action, *args, **kwargs):
    def apply(*_, **__):
        return action(*args, **kwargs)

    return apply

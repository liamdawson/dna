import shutil

try:
    from urllib.parse import urlparse, urlencode
    from urllib.request import urlopen, Request, FancyURLopener
    from urllib.error import HTTPError
except ImportError:
    from urlparse import urlparse
    from urllib import urlencode, FancyURLopener
    from urllib2 import urlopen, Request, HTTPError


def download_file(source, destination):
    opener = FancyURLopener()
    try:
        with opener.open(source) as src:
            with open(destination, "wb") as dest:
                dest.truncate()
                shutil.copyfileobj(src, dest)
    except HTTPError:
        return False
    return True

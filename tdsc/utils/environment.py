import platform


def _dequote(it):
    if it.startswith('"') and it.endswith('"'):
        return it[1:-1]
    return it


def _get_key_from_os_release_content(key, os_release_content):
    if os_release_content is None:
        os_release_content = get_os_release_content()

    if os_release_content and key in os_release_content:
        return os_release_content[key]

    return None


def get_os_release_content():
    if platform.system() != "Linux":
        return {}

    try:
        with open("/etc/os-release", "r") as release_file:
            key_vals = [
                line.split("=", 1) for line in release_file.read().splitlines(False)
            ]
            release_values = {key: _dequote(val) for [key, val] in key_vals}

            return release_values
    except IOError:
        # this system doesn't have an accessible /etc/os-release
        pass

    return {}


def get_linux_distro_codename(os_release_content=None):
    return _get_key_from_os_release_content("VERSION_CODENAME", os_release_content)


def get_linux_distro_version(os_release_content=None):
    return _get_key_from_os_release_content("VERSION_ID", os_release_content)


def get_linux_distro_id(os_release_content=None):
    return _get_key_from_os_release_content("ID", os_release_content)

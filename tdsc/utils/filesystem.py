import os
import errno
from shutil import copyfile


def get_cache_dir(*segments):
    cache_dir = os.path.join(os.path.expandvars('$HOME'), '.cache', 'tdsc')
    if segments:
        cache_dir = os.path.join(cache_dir, *segments)

    ensure_directory(cache_dir)
    return cache_dir


def set_file_text_content(destination, content):
    with open(destination, "w") as f:
        f.truncate()
        f.write(content)
    return True


def set_file_binary_content(destination, content):
    with open(destination, "wb") as f:
        f.truncate()
        f.write(content)
    return True


def copy_file_content(source, destination):
    copyfile(source, destination)
    return True


def ensure_directory(destination, mode=0o755):
    try:
        os.makedirs(destination)
        os.chmod(destination, mode)
    except OSError as e:
        if e.errno == errno.EEXIST and os.path.isdir(destination):
            pass
        else:
            raise
    return True

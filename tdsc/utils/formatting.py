def indent(size, value):
    return "\n".join(["{}{}".format(" " * size, line) for line in value.splitlines()])

import zipfile


def extract_zip(source, destination):
    archive = zipfile.ZipFile(source)
    archive.extractall(destination)

    return True

from .utils.formatting import indent


def describe_action(value, action):
    def wrapper(*args, **kwargs):
        print(value)
        return action(*args, **kwargs)

    return describe(value, wrapper, getattr(action, "description", None))


def describe(value, action, current_description=None):
    if not current_description:
        try:
            current_description = getattr(
                action, "description", action.__dict__["description"]
            )
        except KeyError:
            current_description = None

    new_description = value

    if current_description:
        new_description += "\n{}".format(indent(1, current_description))

    action.__dict__.update({"description": new_description})
    return action

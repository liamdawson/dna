class Context(object):
    def __init__(self):
        self.tags = set()

    def add_tag(self, tag):
        self.tags.add(tag)
        return self

    def add_tags(self, *tags):
        for tag in tags:
            self.add_tag(tag)
        return self

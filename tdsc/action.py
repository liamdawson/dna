class Action(object):
    def __init__(self, *children):
        self.children = children
        self.description = "\n".join(
            filter(
                None, [getattr(child, "description", None) for child in self.children]
            )
        )

    def __call__(self, *args, **kwargs):
        for child in self.children:
            if not child(*args, **kwargs) is True:
                return False
        return True


def action(*actions):
    return Action(*actions)

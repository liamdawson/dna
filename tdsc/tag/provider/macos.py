import platform


def get_macos_tags():
    """Sets 'macos', 'macos:version' tags where possible.

    The 'macos' tag is guaranteed to be set if the system is detected as macOS by Python. The macos:version
    tag will also be set based on Python's ability to detect it."""

    if not platform.system() == "Darwin":
        return set()

    tags = {"macos"}

    mac_ver = platform.mac_ver()

    if not mac_ver or not mac_ver[0]:
        return tags

    release_version = ".".join(mac_ver[0].split(".")[:2])

    return tags.union({"macos:{}".format(release_version)})

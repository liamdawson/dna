import platform


def get_windows_tags():
    """Sets 'windows', 'windows:version' tags where possible.

    The 'windows' and 'windows:version' tags are guaranteed to be set if the system is detected as Windows by Python.
    """
    if not platform.system() == "Windows":
        return set()

    version = ".".join(platform.win32_ver()[1].split(".")[:2])

    return {"windows", "windows:{}".format(version)}

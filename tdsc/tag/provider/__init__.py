from . import linux, macos, windows, all


def get_os_tags():
    return linux.get_linux_tags().union(
        macos.get_macos_tags().union(windows.get_windows_tags())
    )


def get_conventional_tags():
    return all.get_all_tag().union(get_os_tags())

from ...utils.environment import (
    get_os_release_content,
    get_linux_distro_id,
    get_linux_distro_codename,
    get_linux_distro_version,
)
import platform


def _dequote(it):
    if it.startswith('"') and it.endswith('"'):
        return it[1:-1]
    return it


def get_linux_tags():
    """Sets 'linux', 'linux:distro', 'distro:version' and 'distro:codename' tags where possible.

    The 'linux' tag is guaranteed to be set if the system is detected as Linux by Python. The other
    tags are dependent on the presence of fields in /etc/os-release."""

    if not platform.system() == "Linux":
        return set()

    tags = {"linux"}

    os_release = get_os_release_content()
    distro_id = get_linux_distro_id(os_release)
    distro_codename = get_linux_distro_codename(os_release)
    distro_version = get_linux_distro_version(os_release)

    if distro_id:
        tags.add("linux:{}".format(distro_id))

    if distro_id and distro_codename:
        tags.add("{}:{}".format(distro_id, distro_codename))

    if distro_id and distro_version:
        tags.add("{}:{}".format(distro_id, distro_version))

    return tags

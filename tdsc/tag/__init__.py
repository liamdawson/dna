from ..utils.formatting import indent


def require_tag(tag, action):
    """Run the underlying action if the tag requirement is met"""

    def with_tag(*args, **kwargs):
        try:
            if tag not in args[0].tags:
                return True
        except TypeError:
            return True

        return action(*args, **kwargs)

    inner_description = getattr(action, "description", None)

    if inner_description:
        with_tag.__dict__.update(
            {"description": "If `{}`:\n{}".format(tag, indent(1, inner_description))}
        )

    return with_tag


def expand_tags(tags, expansions, extractions):
    new_tags = set(tags)

    changed = True

    while changed:
        current_tags = set(new_tags)

        for tag in current_tags:
            if tag in expansions.keys():
                new_tags = new_tags.union(set(expansions[tag]))

            for key in extractions:
                if tag in extractions[key]:
                    new_tags.add(key)

        changed = not current_tags == new_tags

    return new_tags

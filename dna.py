import argparse
import importlib
from tdsc.prelude import expand_tags, DesiredStateContext
from tdsc.tag.provider import get_conventional_tags

tag_expansions = {
    "xps15": ["no-wayland", "nvidia-proprietary"],
    "linux:ubuntu": ["gnome"],
}

tag_extractions = {
    "apt": ["linux:ubuntu", "linux:debian"],
    "gui": ["gnome"],
    "zsh": ["linux", "macos"],
}

parser = argparse.ArgumentParser(prog="dna-apply")
parser.add_argument(
    "target", metavar="TARGET", help="state application target (e.g. os, user)"
)
parser.add_argument("tags", metavar="TAG", nargs="*", help="tags for target state")
parser.add_argument(
    "--dry-run", action="store_true", default=False, help="don't actually apply states"
)

if __name__ == "__main__":
    result = parser.parse_args()

    tags = set(result.tags).union(get_conventional_tags())
    tags = expand_tags(tags, tag_expansions, tag_extractions)

    context = DesiredStateContext()
    context.add_tags(*tags)

    state_module = None
    try:
        state_module = importlib.import_module("{}_state".format(result.target))
    except ImportError:
        print("State target {} could not be found.".format(result.target))
        raise

    root_state = getattr(state_module, "state", None)

    if root_state:
        print(
            "Here's what this state will do:\n\n{}".format(
                getattr(root_state, "description", "<State has no description>")
            )
        )

        if not result.dry_run:
            if root_state(context) is True:
                print("All states successfully applied.")
            else:
                print(
                    " *** An error occurred. States were not all successfully applied. *** "
                )
    else:
        print("State target {} did not contain a root state.".format(result.target))
